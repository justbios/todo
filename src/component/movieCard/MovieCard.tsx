import React, { FC } from "react";
import style from "./movieCard.module.css";

interface IMovieCardProps {
  url: string;
  name: string;
  date: string;
  rating: string | number;
}

export const MovieCard: FC<IMovieCardProps> = ({ url, name, date, rating }) => {
  return (
    <div className={style.container}>
      <div className={style.card_wrapper}>
        <div className={style.image_wrapper}>
          <img src={url} className={style.image} />
        </div>
        <div className={style.short_movie_info}>
          <div className={style.movie_name}>{name}</div>
          <div className={style.movie_info}>
            <div className={style.card_info_item}>
              <div className={style.label_info}>Release date</div>
              <div>{date}</div>
            </div>
            <div className={style.card_info_item}>
              <div className={style.label_info}>Rating</div>
              <div>
                <div className={style.icon_imdb}>IMDb</div>
                <span className={style.rating}>{rating}</span>/10
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
