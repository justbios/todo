import { type FC } from "react";
import type React from "react";
import { BiSearchAlt2 } from "react-icons/bi";
import style from "./input.module.css";

interface ISearchBarProps {
  type?: string;
  placeholder: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onBlur?: (e: React.FormEvent<HTMLInputElement>) => void;
  search?: boolean;
  label?: string;
  error?: string;
  value?: string;
  name?: string;
  id?: string;
}

export const Input: FC<ISearchBarProps> = ({
  type,
  placeholder,
  onChange,
  search,
  error,
  value,
  name,
  onBlur,
  id,
  label,
}) => {
  return (
    <div className={style.inputWrapper}>
      <label htmlFor={id} className={style.label}>
        {label}
      </label>

      <div className={style.searchContainer}>
        <input
          className={style.searchInput}
          name={name}
          value={value}
          type={type}
          placeholder={placeholder}
          onChange={onChange}
          id={id}
          onBlur={onBlur}
        />
        {search && <BiSearchAlt2 className={style.searchIcon} />}
      </div>
      {error && <div className={style.error}>{error}</div>}
    </div>
  );
};
