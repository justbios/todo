import React, { FC } from "react";
import styles from './pagination.module.css'
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";
import { usePagination } from "../../hooks/usePagination";
import classNames from "classnames";


interface IPaginationProps {
    onPageChange: (page: number) => void;
    currentPage: number;
    totalCount: number;
    pageSize: number;
};

export const Pagination:FC<IPaginationProps> = ({onPageChange, currentPage, totalCount, pageSize}) => {
    const paginationRange = usePagination({
        currentPage,
        totalCount,
        siblingCount: 1,
        pageSize
      });

    const disabledNext = paginationRange && currentPage ===  paginationRange[paginationRange?.length - 1];
    const disabledPrev = currentPage === 1

      const nextPage = () => {
        if(disabledNext) return;
        onPageChange(currentPage + 1)  
      };
    
      const prevPage = () => {
        if(disabledPrev) return;
        onPageChange(currentPage - 1)  
      };

      if (!totalCount || !pageSize) return null

    return (
        <div className={styles.container}>
            <div className={classNames(styles.item, disabledPrev && styles.disabled)}>
                <BsChevronLeft onClick={prevPage}  />
            </div>
            {paginationRange?.map((page) => {
                  if (page === '...') {
                    return <div className={styles.item}>&#8230;</div>;
                  }
            return(
                <div className={styles.item}
                onClick={() => onPageChange(+page)}
                style={{ background: currentPage === page ? '#61A3BA' : '' }}
                >
                    {page}
                </div>
            )})}
            <div className={classNames(styles.item,  disabledNext && styles.disabled)}>
                <BsChevronRight onClick={nextPage} />
            </div>
        </div>
    )
}