import { type FC } from 'react'
import type React from 'react'
import style from './Button.module.css'
import cn from 'classnames'
import { ThreeDots } from 'react-loader-spinner'

interface IButtonProps {
  isLoading?: boolean
  disabled?: boolean
  onClick: (e: React.MouseEvent<HTMLElement>) => void
  title: string
}

export const Button: FC<IButtonProps> = ({
  disabled,
  isLoading,
  onClick,
  title
}) => {
  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    if (disabled || isLoading) {
      return
    }

    onClick(e)
  }

  return (
    <div>
      <div
        className={cn(style.standard, disabled && style.disabled)}
        onClick={handleClick}
      >
        {isLoading
          ? (
          <ThreeDots
            height="20"
            width="30"
            color="#fff"
            ariaLabel="three-dots-loading"
            visible={true}
          />
            )
          : (
              title
            )}
      </div>
    </div>
  )
}
