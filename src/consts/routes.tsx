import { Navigate, Route, Routes, useNavigate } from "react-router-dom";
import { SignIn } from "../pages/SignIn/SignIn";
import { SignUp } from "../pages/SignUp/SignUp";
import { useEffect } from "react";
import { useAuth } from "../component/AuthProvider/AuthProvider";
import { Movies } from "../pages/movies/Movies";

export enum routesPath {
  /// AUTH
  SignIn = "/signIn",
  SignUp = "/signUp",
  // APP
  Movies = '/movies',
}

export default  () => {
  const {isAuth, setIsAuth} = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if(token) {
      setIsAuth(!!token);
      navigate(routesPath.Movies)
    } else {
      navigate(routesPath.SignIn)
    }
  }, [isAuth]);


    return (
    <Routes>
        <Route path={routesPath.SignIn} element={<SignIn />} />
        <Route path={routesPath.SignUp} element={<SignUp />} />
        {isAuth && <Route path={routesPath.Movies}  element={<Movies />} />}
        <Route path="*" element={<Navigate to={routesPath.SignIn} replace />} />
    </Routes>
    )
};
