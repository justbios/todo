import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Routes from "./consts/routes";
import { AuthProvider } from "./component/AuthProvider/AuthProvider";

function App() {
  return (
        <AuthProvider>
          <BrowserRouter>
            <Routes />
          </BrowserRouter>
        </AuthProvider>
  );
}

export default App;
