import axios from "axios";
import { API_KEY } from "../utils/baseUrls";

export const getMovies = async (page?: number, searchQuery?: string) => {
  const url = searchQuery ? 
  `https://api.themoviedb.org/3/search/movie?query=${searchQuery}&page=${page}&api_key=${API_KEY}` 
  :
  `https://api.themoviedb.org/3/trending/all/day?api_key=${API_KEY}&page=${page}`

  const { data } = await axios({
    method: "GET",
    url: url,
    headers: {
      "Content-Type": "application/json",
    },
  });
  return data;
};
