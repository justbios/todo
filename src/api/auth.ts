import axios from "axios"
import { BASE_URL } from "../utils/baseUrls";

interface IDataLogin {
    email: string;
    password: string;
}

interface IDataRegister extends IDataLogin {
    username: string;
}

export const login = async (loginData: IDataLogin) => {
    const {data} = await axios({
        method: "POST",
        baseURL: BASE_URL,
        url: '/auth/login',
        headers: {
            'Content-Type': 'application/json',
        },
        data: loginData
    })
    return data
}

export const register = async (registerData: IDataRegister) => {
    const {data} = await axios({
        method: "POST",
        baseURL: BASE_URL,
        url: '/auth/registration',
        headers: {
            'Content-Type': 'application/json',
        },
        data: registerData
    })
    return data
}