import React from "react";

export const shortString = (str: string = '', maxLength: number = 55): string => {
  if (str.length > maxLength) {
    return str.slice(0, maxLength) + "...";
  }

  return str;
}