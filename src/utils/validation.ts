import * as yup from 'yup'

export const signUpValidationSchema = yup.object().shape({
  name: yup.string().required('Name is required'),
  email: yup.string().email().required('Email is required'),
  password: yup.string().min(4).required('Password is required'),
  confirmPassword: yup
    .string()
    .required('Confirm Password is required')
    .oneOf([yup.ref('password'), ''], 'Password must match')
})

export const signInValidationSchema = yup.object().shape({
  email: yup
    .string()
    .required('Required')
    .email('Please provide a valid Email'),
  password: yup
    .string()
    .required('Required')
    .min(4, 'Password is too short - should be 4 chars minimum.')
})
