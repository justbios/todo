import React, { FC, useState } from "react";
import { Input } from "../../component/Input/Input";
import { Button } from "../../component/button/Button";
import style from "./signup.module.css";
import { useFormik } from "formik";
import { signUpValidationSchema } from "../../utils/validation";
import { useNavigate } from "react-router-dom";
import { routesPath } from "../../consts/routes";
import { register } from "../../api/auth";
import { useAuth } from "../../component/AuthProvider/AuthProvider";

export const SignUp: FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const navigate = useNavigate();
  const {setIsAuth} = useAuth();

  const { values, handleSubmit, handleBlur, handleChange, errors, touched } =
    useFormik({
      initialValues: {
        name: "",
        email: "",
        password: "",
        confirmPassword: "",
      },
      onSubmit: async ({email, password, name}) => {
        try {
          setLoading(true);
          const {token} = await register({email, password, username: name})
          if(token) {
            localStorage.setItem("token", token)
            setIsAuth(!!token);
            setLoading(false)
            navigate(routesPath.Movies)
          }
        } catch (e) {
          setLoading(false)
          console.log(e)
        }
      },
  
      validationSchema: signUpValidationSchema,
    });

  const goToSignIn = () => {
    navigate(routesPath.SignIn);
  };

  return (
    <div className="App">
      <header className="App-header">
        <div className={style.signUp}>
          <h1>Sign Up</h1>
          <Input
            id="name"
            name={"name"}
            placeholder={"Name"}
            onChange={handleChange}
            onBlur={handleBlur}
            type={"name"}
            label={"Name"}
            value={values.name}
            error={errors.name && touched.name ? errors.name : ""}
          />
          <Input
            id="email"
            name={"email"}
            placeholder={"E-mail"}
            onChange={handleChange}
            onBlur={handleBlur}
            type={"email"}
            label={"Email"}
            value={values.email}
            error={errors.email && touched.email ? errors.email : ""}
          />
          <Input
            id="password"
            name={"password"}
            placeholder={"Password"}
            onChange={handleChange}
            onBlur={handleBlur}
            type={"password"}
            label={"Password"}
            value={values.password}
            error={errors.password && touched.password ? errors.password : ""}
          />
          <Input
            id="confirm_password"
            name={"confirmPassword"}
            placeholder={"Confirm Password"}
            onChange={handleChange}
            onBlur={handleBlur}
            type={"password"}
            label={"Confirm Password"}
            value={values.confirmPassword}
            error={
              errors.confirmPassword && touched.confirmPassword
                ? errors.confirmPassword
                : ""
            }
          />
          <Button onClick={() => handleSubmit()} title="Sign Up" />
          <div className={style.link} onClick={goToSignIn}>
            Already have an account? Sign in here 
          </div>
        </div>
      </header>
    </div>
  );
};
