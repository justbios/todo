import { MovieCard } from "../../component/movieCard/MovieCard";
import style from "./movies.module.css";
import { Input } from "../../component/Input/Input";
import { useEffect, useState, useCallback } from "react";
import { getMovies } from "../../api/movies";
import debounce from "lodash.debounce";
import { ThreeDots } from "react-loader-spinner";
import { Pagination } from "../../component/Pagination/Pagination";
import { IMAGE_BASE_URL, IMAGE_DEFAULT_URL } from "../../utils/baseUrls";
import { shortString } from "../../utils/nameSlice";

interface IMovie {
  title: string;
  name: string;
  vote_average: number;
  id: string | number;
  release_date: string;
  first_air_date: string;
  poster_path: string;
  runtime: string;
}

export const Movies = () => {
  const [movies, setMovies] = useState([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [totalPage, setTotalPage] = useState<number>(0);
  const [loading, setLoading] = useState<boolean>(false);
  const [searchParams, setSearchParams] = useState<string>("");

  useEffect(() => {
    setLoading(true);
    getMovies(currentPage, searchParams)
      .then(({ results, total_pages }) => {
        if (results.length === 0) {
          alert("no found movies");
          return;
        }
        setMovies(results);
        setTotalPage(total_pages);
        setLoading(false);
      })
      .catch((err) => {
        console.log("error", err);
        setLoading(false);
      });
  }, [currentPage, searchParams]);

  const onSearch = (e: React.ChangeEvent<HTMLInputElement>) =>
    setSearchParams(e.target.value);

  const debouncedChangeHandler = useCallback(debounce(onSearch, 500), []);

  return (
    <div className={style.container}>
      <Input
        placeholder={"Search"}
        onChange={debouncedChangeHandler}
        search={true}
      />

      <div className={style.movie_wrapper}>
        {!loading ? (
          movies.map((movie: IMovie) => {
          const imageUrl = movie.poster_path ? IMAGE_BASE_URL + movie.poster_path :  IMAGE_DEFAULT_URL
          return (
            <MovieCard
              url={imageUrl}
              name={shortString(movie.name || movie.title)}
              rating={+movie.vote_average.toFixed(1)}
              date={movie.release_date || movie.first_air_date}
              key={movie.id}
            />
          )})
        ) : (
          <ThreeDots
            height="20"
            width="30"
            color="#fff"
            ariaLabel="three-dots-loading"
            visible={true}
          />
        )}
      </div>

      <div className={style.paginationContainer}>
        <Pagination
          currentPage={currentPage}
          totalCount={totalPage}
          pageSize={10}
          onPageChange={setCurrentPage}
        />
      </div>
    </div>
  );
};
