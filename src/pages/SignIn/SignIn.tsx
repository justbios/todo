import React, { useState, type FC } from "react";
import { Input } from "../../component/Input/Input";
import { Button } from "../../component/button/Button";
import style from "./signIn.module.css";
import { useFormik } from "formik";
import { signInValidationSchema } from "../../utils/validation";
import { login } from "../../api/auth";
import { useAuth } from "../../component/AuthProvider/AuthProvider";
import { useNavigate } from "react-router-dom";
import { routesPath } from "../../consts/routes";

export const SignIn: FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const { setIsAuth } = useAuth();
  const navigate = useNavigate();

  const { values, handleSubmit, handleBlur, handleChange, errors, touched } =
    useFormik({
      initialValues: {
        email: "",
        password: "",
      },

      onSubmit: async (values) => {
        try {
          setLoading(true);
          const { token } = await login(values);
          if (token) {
            setLoading(false)
            localStorage.setItem("token", token);
            setIsAuth(!!token);
            navigate(routesPath.Movies);
          }
        } catch (err) {
          setLoading(false)
          console.log(err);
        }
      },
      validationSchema: signInValidationSchema,
    });

  const goToSignUp = () => {
    navigate(routesPath.SignUp);
  };

  return (
    <div className="App">
      <header className="App-header">
        {" "}
        <div className={style.signIn}>
          <h1>Sign In</h1>
          <Input
            id="email"
            onBlur={handleBlur}
            name={"email"}
            placeholder={"E-mail"}
            onChange={handleChange}
            type={"email"}
            label={"Email"}
            value={values.email}
            error={errors.email && touched.email ? errors.email : ""}
          />
          <Input
            id="password"
            onBlur={handleBlur}
            name={"password"}
            placeholder={"Password"}
            onChange={handleChange}
            type={"password"}
            label={"Password"}
            value={values.password}
            error={errors.password && touched.password ? errors.password : ""}
          />
          <Button onClick={() => handleSubmit()} title="Sign in" isLoading={loading} />
          <div className={style.link} onClick={goToSignUp}>Don't have an account</div>
        </div>
      </header>
    </div>
  );
};
