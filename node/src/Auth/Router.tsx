const AuthRouter = require('express');
const controller = require('./controller.tsx');
const router = new AuthRouter();
const {check} = require("express-validator")
const authMiddleware = require('./middleware.ts')


router.post('/auth/registration', [
    check('username', "The username cannot be empty").notEmpty(),
    check('email', "The field must be an email").trim().isEmail(),
    check('password', "Password must be greater than 4 and less than 10 characters").isLength({min:4, max:10})
], controller.registration)
router.post('/auth/login', controller.login)
router.get('/auth/users', authMiddleware, controller.getUsers)

module.exports = router;