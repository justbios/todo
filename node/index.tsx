const express = require('express');
const mongoose = require('mongoose');
const authRouter  = require('./src/Auth/Router.tsx');
const cors = require('cors');
const {uri} = require('./config.ts')
const { MongoClient } = require("mongodb");
const { ObjectId } = require('mongodb');

//40875a7f166b061faf5f77ac964541b9

const PORT = process.env.PORT || 5001;
const client = new MongoClient(uri);
const app = express();
app.use(cors());
app.use(express.json());

app.use('/api', authRouter)
app.get('/api/movies', async (req, res) => {
    try {
        const database = await client.db('sample_mflix');
        const movies = await database.collection('movies');
        const movie = await movies.find({}).limit(50).sort(["year", -1]).toArray();
        res.status(200).json({data: movie, message: 'success'})
    } catch (err) {
        res.status(400).json({message: 'get Movie error', err})
    }
})

const start = async () => {
    try {
        await mongoose.connect(uri);
        app.listen(PORT, () => console.log('listening on port'));
    } catch (e) {
        console.log(e)
    }
};

start();